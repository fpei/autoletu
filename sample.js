var casper = require('casper').create();
var colorizer = require('colorizer').create('Colorizer');
var x = require('casper').selectXPath;

var json  = require('./user.json');
// require('utils').dump(json);
casper.echo('AutoLetu Version：'+json.version);
casper.echo('正在打开letu网站. Opening the letu website...', 'INFO');

casper.start('http://letu.ru/', function() {
    this.echo(this.getTitle());
    this.fill('form[id="loginForm"]', {
//        'login': '323218955@163.com'
//        "username": "924282818@qq.com",
        'login': json.username
    }, false);
    this.fill('form[id="loginForm"]', {
        'password': json.password
    }, false);
});

//click login buttion
casper.then(function() {
    this.click('input[id="loginEnterButton"]');
    // ** give my crappy dev server several seconds to respond
    casper.echo('使用用户名('+ json.username+')登录网站中，请稍等. Using username('+ json.username+') to login the website...', 'INFO');
    if(json.capture){
        this.wait(300,function() {
            this.capture("./capture/1.png");
        });
    }
    else{
        this.wait(300,function() {
        });
    }
});

var urls =[];
var wishlistUrl = 'https://www.letu.ru/cart/wishList.jsp';
casper.thenOpen(wishlistUrl,function(){
    var id  = this.getElementAttribute('input[name="/atg/commerce/gifts/GiftlistFormHandler.giftlistId"]', 'value')
    casper.echo('user id: '+id);
    if(id==null){
        casper.echo('登录失败, 请重新启动程序. Login failed, please re-open this program.','ERROR');
        this.exit();
    }
    urls = this.evaluate(getLinks);
    urls[0]=wishlistUrl;
    urls.pop();  //deleter lastone, for replicated item like 11
    for(var i =1;i<urls.length;i++){
        urls[i]= "https://www.letu.ru" +urls[i];
        casper.echo("找到心愿单, Found wishlist urls:" + urls[i]);
    }
    if(json.capture){
        this.wait(300,function() {
            this.capture("./capture/2.png");
        });
    }
});


// main loop start
casper.then(function check(){
    this.repeat(json.refreshTimes,function () {
        casper.each(urls, function(self, url) {
            self.thenOpen(url, function(){
                this.echo('Opening: ' + url, 'INFO');
                casper.clickWhileSelector('(//input[@name = "addButton"])',1);
            });
        });
        if(json.capture){
            this.wait(300,function() {
                this.capture("./capture/3.png");
            });
        }

    // fake_checkout and conform the address
        this.echo('正在检查购物车商品情况, Opening cart...', 'INFO');
        this.thenOpen("https://www.letu.ru/cart/cart.jsp", function () {
            if (this.exists(x('(//tr[@class = "awayOrange"]//input[@class="atg_store_textButton atg_store_actionDelete"])'))) {
                casper.echo("发现不可购买货物, Found unsalable goods!", 'COMMENT');
                this.repeat(1, function () {
                    // casper.clickWhileSelector('(//tr[@class = "awayOrange"]//input[@class="atg_store_textButton atg_store_actionDelete"])');
                    casper.clickWhileSelector('(//tr[@class = "awayOrange"]', 1, '//input[@class="atg_store_textButton atg_store_actionDelete"])');
                    casper.clickWhileSelector('(//input[@id = "deleteApprove"])', 1);
                });
            }


            if(!this.exists(x('(//*[@id="atg_store_cart"]/tbody/tr[@*]//input[@class="text qty atg_store_numericInput "] )'))){
                casper.echo('购物车没有商品, 继续刷单...', 'INFO');
            }
            else{
                // this function can influence the getting value method
                casper.echo('正在设置下单数量, setting order number...', 'INFO');
                this.setMultiKeys('//*[@id="atg_store_cart"]/tbody/tr[@*]', json.orderNum, 1, '//input[@class="text qty atg_store_numericInput "]');

                //check the goods number and go to check out
                this.then(function () {
                    var goodsNum;
                    var linksNum
                    var firstNum;
                    linksNum = this.evaluate(function () {
                        return document.querySelectorAll('div[class="h2-like name"]').length;
                        //#atg_store_cart > tbody > tr:nth-child(15) > td.item > div
                        // return document.querySelectorAll('#atg_store_cart > tbody > tr > td.item > "div.h2-like name"').length;
                    });
                    casper.echo('linksNum = '+linksNum);
                    if (linksNum != 0) {
                        firstNum = this.evaluate(function () {
                            // var firstNum = this.getElementAttribute('input[class="atg_store_numericInput"]', 'value');
                            // var firstNum = this.getElementAttribute(x('//*[@id="atg_store_cart"]/tbody/tr[@*]//input[@class="text qty atg_store_numericInput "]'), 'value');
                            return document.querySelector('input[class="text qty atg_store_numericInput "]').value;
                        });
                        casper.echo('firstNum = '+firstNum);
                        if (firstNum >= linksNum) {
                            goodsNum = firstNum;
                        }
                    }
                    else {
                        goodsNum = linksNum;
                    }

                    console.log('货物数量大于等于' + goodsNum + ', Goods number is more than ' + goodsNum);
                    if ((goodsNum >= json.goodsNum) && (!this.exists(x('(//tr[@class = "awayOrange"]//input[@class="atg_store_textButton atg_store_actionDelete"])')))) {

                        // casper.echo('正在设置下单数量, setting order number...', 'INFO');
                        // this.setMultiKeys('//*[@id="atg_store_cart"]/tbody/tr[@*]', json.orderNum, 1, '//input[@class="text qty atg_store_numericInput "]');

                        if (json.capture) {
                            this.wait(300, function () {
                                this.capture("./capture/3-1.png");
                            });
                        }
                        casper.echo("开始下单, Start to checkout...", 'INFO');
                        this.click('button[id="fake_checkout"]');
                        this.wait(json.clickSpeed, function () {
                        });

                        //check cart error
                        if (this.exists(x('(//div[@class = "errorMessage"])'))) {
                            casper.echo("购物车中发现错误，请检查购物车! There are some errors in your cart, please check it!!!!!!!!", 'ERROR');
                            this.keep_looping = false;
                        }
                        if (json.capture) {
                            this.wait(300, function () {
                                this.capture("./capture/4.png");
                            });
                        }

                        this.thenOpen("https://www.letu.ru/checkout/checkout.jsp", function () {
                            casper.echo("已打开下单链接.Opened the checkout page.", "INFO")
                            // casper.clickWhileSelector(x('//label[@for="inShop" and @class="nc-shipping-type-btn_label"]'),1);
                            if (this.exists('input[type = "radio"][id="express"]')) {
                                this.click('input[type = "radio"][id="express"]');
                            }
                            if (json.capture) {
                                this.wait(300, function () {
                                    this.capture("./capture/5.png");
                                });
                            }
                            if (json.ShippingTime != 0) {
                                this.echo('正在设置送货时间, The shipping time have been customized, setting shipping time...', 'INFO');
                                this.sendKeys('input[id="deliveryDate"]', json.ShippingTime);
                            }
                            if (!json.testMode) {
                                //*[@id="button_continue_checkout"]
                                this.click('input[id="button_continue_checkout"]');
                                if (json.capture) {
                                    this.wait(1, function () {
                                        this.capture("./capture/5-1.png");
                                    });
                                }
                                this.wait(1000, function () {
                                    // for the consistence of cart
                                });
                                casper.echo('下单成功! Check out successfully!', 'GREEN_BAR');
                            }
                            if (json.capture) {
                                this.wait(300, function () {
                                    this.capture("./capture/6.png");
                                });
                            }
                        });
                    }
                });
            }


        });
    });
});


//
// function getClicks() {
//     var btns = document.querySelectorAll('input[name = "addButton"]');
//     for(var i =0;i<btns.length;i++){
//         var e = document.createEvent('MouseEvents');
//         e.initMouseEvent('click', true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
//         btns[i].dispatchEvent(e);
//         // goods[i]=btn[i].getAttribute('data-e-commerce');
//         //  this.wait(300,function() {
//         //  });
//     }
//     return Array.prototype.map.call(btns, function(e) {
//         return e.getAttribute('data-e-commerce');
//     });
// };

casper.clickWhileSelector = function(selector, i, moreSelector) {
    return this.then(function() {
        i = i || 1;
        moreSelector = moreSelector||'';
        var j=i+1;
        var selectorNth = selector+'[' + i + ']'+moreSelector;
        var selectorNNth = selector+'[' + j + ']'+moreSelector;
        // var x = require('casper').selectXPath;
        if (this.exists(x(selectorNth))) {
            this.echo('found link: '+this.getElementInfo(x(selectorNth)).tag);
            this.click(x(selectorNth));

            this.wait(json.clickSpeed,function() {  //changed based on network speed
            });
            if(this.exists(x(selectorNNth))) {
                return this.clickWhileSelector(selector, i + 1, moreSelector);
            }
        }
        else{
            casper.echo('没找到新的货, Didnt find link');
        }

    });
}



casper.setMultiKeys = function(selector,num,i,moreSelector){
    this.reload();
    this.then(function () {
        i = i || 1;
        moreSelector = moreSelector||'';
        var j=i+1;
        var eInput = '//input[@class="text qty atg_store_numericInput  error"]';
        selectorNth = selector+'[' + i + ']'+moreSelector;
        selectorNNth = selector+'[' + j + ']'+moreSelector;
        if(this.exists(x(selectorNth))) {
            this.echo('Found input: ' + this.getElementInfo(x(selectorNth)).tag);
            var subNum = num;
            casper.echo("Num: " + num);
            this.sendKeys(x(selectorNth), ""+num);
            if (json.capture) {
                this.capture("./capture/3-1-"+num+".png");
            }
        }

        this.wait(1000,function() {  //changed based on network speed
        });
//  var style =this.getElementAttribute(x('//div[@class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable"]'), 'style');
        var style =this.evaluate(function () {
            return document.querySelector('div[class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable"][aria-labelledby="ui-dialog-title-takeAwayDialog"]').getAttribute('style');
        });
        this.echo("style:"+style);
        //exist error number
        if(style!==null && style.indexOf("block")!== -1){
            //*[@id="takeAwayDialog"]/table/tbody/tr[2]/td[2]/div/span
            this.click(x('//*[@id="takeAwayDialog"]//span[@class = "closeD"]'));
            this.echo("下单数量不可用, 开始重试减半数量！", 'INFO');
            subNum = Math.ceil(subNum/2);
            this.echo("正在设置数量！");
            if(json.capture){
                this.capture("./capture/3-2-"+subNum+".png");
            }
            if(subNum!==1){
                return this.setMultiKeys(selector,""+subNum,i,moreSelector);
            }
        }
        if(this.exists(x(selectorNNth))) {
            return this.setMultiKeys(selector,num,j,moreSelector);
        }
        else{
            casper.echo('selectorNNth not exist: '+selectorNNth);
        }
        return this.echo('Set Keys Finished.');
    });


}



function getLinks() {
    var links = document.querySelectorAll('.pagination ul li a');
    return Array.prototype.map.call(links, function(e) {
        return e.getAttribute('href');
    });
};

casper.then(function close(){
    casper.echo("The loop of refreshing has been run out. Please restart it if you want to keep on refreshing.")
});

casper.run();

