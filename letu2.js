/**
 * Created by fpei on 6/25/17.
 */

var casper = require('casper').create({
    // clientScripts:  [
    //     'includes/jquery.js',      // These two scripts will be injected in remote
    //     'includes/underscore.js'   // DOM on every request
    // ],
    pageSettings: {
        loadImages:  false,        // The WebPage instance used by Casper will
        loadPlugins: false         // use these settings
    },
    // logLevel: "info",              // Only "info" level messages will be logged
    // verbose: true                  // log messages will be printed out to the console
});
var colorizer = require('colorizer').create('Colorizer');
var x = require('casper').selectXPath;
var json  = require('./user.json');
var urls =[];
var wishlistUrl = 'https://www.letu.ru/cart/wishList.jsp';
var urlNumber = 0;
var newItemList = []; //save the item which has been noticed


casper.echo('AutoLetu Version：'+json.version);

casper.echo('正在打开letu网站. Opening the letu website...', 'INFO');
casper.start('http://letu.ru/', function() {
    this.echo(this.getTitle());
    this.fill('form[id="loginForm"]', {
        //'login': '323218955@163.com',"924282818@qq.com"
        'login': json.username,
        'password': json.password
    }, false);
});
casper.then(function () {
    this.click('input[id="loginEnterButton"]');
    casper.echo('使用用户名('+ json.username+')登录网站中，请稍等. Using username('+ json.username+') to login the website...', 'INFO');
    // this.capture("./capture2/1.png");
})

casper.wait(500,function() {});

casper.thenOpen(wishlistUrl,function(){
    var id  = this.getElementAttribute('input[name="/atg/commerce/gifts/GiftlistFormHandler.giftlistId"]', 'value')
    if(id==null){
        casper.echo('登录失败, 请重新启动程序. Login failed, please re-open this program.','ERROR');
        this.exit();
    }
    else{
        casper.echo('登录成功, user id: '+id);
    }

    urls = this.evaluate(getLinks);
    urls[0]=wishlistUrl;
    urls.pop();  //deleter lastone, for replicated item like 11
    for(var i =1;i<urls.length;i++){
        urls[i]= "https://www.letu.ru" +urls[i];
        casper.echo("找到心愿单, Found wishlist urls:" + urls[i]);
    }
    // this.capture("./capture2/2.png");
});

casper.repeat(json.refreshTimes,function () {
    urlNumber = (urlNumber+1)%urls.length;
    url = urls[urlNumber];
// var url =urls[getRandomItem(urls.length)-1];
    this.thenOpen(url, function(){
        var date = new Date(Date.now());
        this.echo('Date: '+date.toString()+'\n Opening: ' + url, 'INFO');
        // casper.clickWhileSelector('(//input[@name = "addButton"])',1);
        var addNum = this.evaluate(function () {
            return document.querySelectorAll('input[name = "addButton"]').length;
        });
        this.echo('心愿单中可下单商品数量： addNum: '+addNum);
        this.then(function() {
            // this.capture("./capture2/3-1.png");
            if(addNum>0){
                // var xpath = '(//input[@name = "addButton"]['+(getRandomItem(addNum))+'])';
                var radNum = getRandomItem(addNum);
                var xpath = '((//input[@name = "addButton"])['+radNum+'])';
                var newItem = this.getElementInfo(x('((//input[@name = "addButton"])['+radNum+']'+'/../../../../..//h3[@class = "title"])')).tag.toString();
                var newDesc = this.getElementInfo(x('((//input[@name = "addButton"])['+radNum+']'+'/../../../../..//p[@class = "description"])')).tag.toString();
                var combine = newItem + newDesc;
                this.echo('刷到一件货物, found link: '+ combine);
                if(newItemList.indexOf(combine)==-1){
                    newItemList.push(combine);
                    this.sendEmail("刷新到一件货物", combine);
                }
                this.thenClick(x(xpath));
                
                // this.wait(300,function() {
                //     this.capture("./capture2/3-2.png");
                // });

                // fake_checkout and conform the address
                this.echo('正在检查购物车商品情况, Opening cart...', 'INFO');
                this.thenOpen("https://www.letu.ru/cart/cart.jsp", function () {
                    if (this.exists(x('(//tr[@class = "awayOrange"]//input[@class="atg_store_textButton atg_store_actionDelete"])'))) {
                        casper.echo("发现不可购买货物, Found unsalable goods!", 'COMMENT');
                        this.repeat(1, function () {
                            // casper.clickWhileSelector('(//tr[@class = "awayOrange"]//input[@class="atg_store_textButton atg_store_actionDelete"])');
                            casper.clickWhileSelector('(//tr[@class = "awayOrange"]', 1, '//input[@class="atg_store_textButton atg_store_actionDelete"])');
                            casper.clickWhileSelector('(//input[@id = "deleteApprove"])', 1);
                        });
                    }
                    else{
                        if(!this.exists(x('(//*[@id="atg_store_cart"]/tbody/tr[@*]//input[@class="text qty atg_store_numericInput "] )'))){
                            casper.echo('购物车没有商品, 继续刷单...', 'INFO');
                        }
                        else {
                            // this function can influence the getting value method
                            casper.echo('正在设置下单数量, setting order number...', 'INFO');
                            this.setMultiKeys('//*[@id="atg_store_cart"]/tbody/tr[@*]', json.orderNum, 1, '//input[@class="text qty atg_store_numericInput "]');

                            //check the goods number and go to check out
                            this.then(function () {
                                var goodsNum;
                                var linksNum
                                var firstNum;
                                linksNum = this.evaluate(function () {
                                    return document.querySelectorAll('div[class="h2-like name"]').length;
                                    //#atg_store_cart > tbody > tr:nth-child(15) > td.item > div
                                    // return document.querySelectorAll('#atg_store_cart > tbody > tr > td.item > "div.h2-like name"').length;
                                });
                                casper.echo('linksNum = ' + linksNum);
                                if (linksNum != 0) {
                                    firstNum = this.evaluate(function () {
                                        // var firstNum = this.getElementAttribute('input[class="atg_store_numericInput"]', 'value');
                                        // var firstNum = this.getElementAttribute(x('//*[@id="atg_store_cart"]/tbody/tr[@*]//input[@class="text qty atg_store_numericInput "]'), 'value');
                                        return document.querySelector('input[class="text qty atg_store_numericInput "]').value;
                                    });
                                    this.waitFor(function check() {
                                        return firstNum != null;
                                    }, function then() {
                                        casper.echo('firstNum = ' + firstNum);
                                        if (firstNum >= linksNum) {
                                            goodsNum = firstNum;
                                        }
                                    },function timeout() {
                                        casper.echo('firstNum time out');
                                            goodsNum = linksNum;
                                    },5000)
                                }
                                else {
                                    goodsNum = linksNum;
                                }
                                this.then(function () {
                                    console.log('货物数量大于等于' + goodsNum + ', Goods number is more than ' + goodsNum);
                                    // if ((goodsNum >= json.goodsNum) && (!this.exists(x('(//tr[@class = "awayOrange"]//input[@class="atg_store_textButton atg_store_actionDelete"])')))) {
                                    if ((goodsNum >= json.goodsNum)) {
                                        if (json.capture) {
                                            this.capture("./capture2/4-1.png");
                                        }
                                        //check cart error
                                        // if (this.exists(x('(//div[@class = "errorMessage"])'))) {
                                        //     casper.echo("购物车中发现错误，请检查购物车! There are some errors in your cart, please check it!!!!!!!!", 'ERROR');
                                        //     // this.keep_looping = false;
                                        // }
                                        // if (json.capture) {
                                        //     this.capture("./capture2/4-2.png");
                                        // }
                                        casper.echo("开始下单, Start to checkout...", 'INFO');
                                        //body/div[@class="globalContainer clearFix"]//div[@class="tabsContainer"]//form[@id="productsCartform"]/div[@class="cartContainer clearfix"]
                                        this.thenClick(x('(//body/div[@class="globalContainer clearFix"]//div[@class="tabsContainer"]//form[@id="productsCartform"]/div[@class="cartContainer clearfix"]//button[@id="fake_checkout"])'));
                                        // this.thenClick('button[id="fake_checkout"]');
                                        this.waitForUrl(/^.*checkout.*/, function then() {
                                            casper.echo("已打开下单链接.Opened the checkout page.", "INFO")
                                            // casper.clickWhileSelector(x('//label[@for="inShop" and @class="nc-shipping-type-btn_label"]'),1);
                                            if (this.exists('input[type = "radio"][id="express"]')) {
                                                this.thenClick('input[type = "radio"][id="express"]');
                                            }
                                            if (json.capture) {
                                                this.capture("./capture2/5.png");
                                            }
                                            if (json.ShippingTime != 0) {
                                                this.echo('正在设置送货时间, The shipping time have been customized, setting shipping time...', 'INFO');
                                                // this.sendKeys('input[id="deliveryDate"]', json.ShippingTime);
                                                //div[@id="shippingPageContainer"]//div[@class="nc-page clearFix"]//div[@data-key="nc-address-date"]/div[@class="nc-step_content"]//input[@id="deliveryDate"]
                                                this.sendKeys(x('(//div[@id="shippingPageContainer"]//div[@class="nc-page clearFix"]//div[@data-key="nc-address-date"]/div[@class="nc-step_content"]//input[@id="deliveryDate"])'), json.ShippingTime,{reset: true});

                                                if (json.capture) {
                                                    this.capture("./capture2/5-1.png");
                                                }
                                            }
                                            if (!json.testMode) {
                                                //*[@id="button_continue_checkobut"]
                                                //form[@id="atg_store_singleCheckoutFormRU"]/div[@class="nc-step nc-step__size__s nc-step__withoutBorder"]/div[@class="nc-step_content"]/div[@class="nc-total"]//input[@id="button_continue_checkout"]
                                                this.thenClick(x('(//form[@id="atg_store_singleCheckoutFormRU"]/div[@class="nc-step nc-step__size__s nc-step__withoutBorder"]/div[@class="nc-step_content"]/div[@class="nc-total"]//input[@id="button_continue_checkout"])'));
                                                // this.thenClick('input[id="button_continue_checkout"]');
                                                this.waitForUrl(/^.*orderConfirmed.*/,function then () {
                                                    casper.echo('下单成功! Check out successfully!', 'GREEN_BAR');
                                                    var date = new Date(Date.now());
                                                    this.capture("./orders/"+date.toString()+".png");
                                                },function timeout() {
                                                    casper.echo('订单确认失败! order confirm unsuccessfully!', 'ERROR');
                                                    //clean cart
                                                    this.thenOpen("https://www.letu.ru/cart/cart.jsp", function () {
                                                        if (this.exists(x('(//input[@class="atg_store_textButton atg_store_actionDelete"])'))) {
                                                            casper.echo("正在清空购物车...");
                                                            this.repeat(1, function () {
                                                                casper.clickWhileSelector('(//tr', 1, '//input[@class="atg_store_textButton atg_store_actionDelete"])');
                                                                casper.clickWhileSelector('(//input[@id = "deleteApprove"])', 1);
                                                            });
                                                        }
                                                    });
                                                },20000)
                                            }
                                        }, function timeout() {
                                            casper.echo('打开下单链接失败! Check out unsuccessfully!', 'ERROR');
                                            //clean cart
                                            this.thenOpen("https://www.letu.ru/cart/cart.jsp", function () {
                                                if (this.exists(x('(//input[@class="atg_store_textButton atg_store_actionDelete"])'))) {
                                                    casper.echo("正在清空购物车...");
                                                    this.repeat(1, function () {
                                                        casper.clickWhileSelector('(//tr', 1, '//input[@class="atg_store_textButton atg_store_actionDelete"])');
                                                        casper.clickWhileSelector('(//input[@id = "deleteApprove"])', 1);
                                                    });
                                                }
                                            });
                                        },10000)
                                    }
                                });
                            });
                        }
                    }
                });
            }
        });
    });
})


// casper.waitForUrl(/^.*_requestid.*/,function () {
//     // this.wait(300,function() {});
//     if(json.capture){
//         this.capture("./capture2/3.png");
//     }
// })



casper.run();

function getRandomItem(num){
    return Math.ceil(num*Math.random());
};

function getLinks() {
    var links = document.querySelectorAll('.pagination ul li a');
    return Array.prototype.map.call(links, function(e) {
        return e.getAttribute('href');
    });
};

casper.clickWhileSelector = function(selector, i, moreSelector) {
    return this.then(function() {
        i = i || 1;
        moreSelector = moreSelector||'';
        var j=i+1;
        var selectorNth = selector+'[' + i + ']'+moreSelector;
        var selectorNNth = selector+'[' + j + ']'+moreSelector;
        // var x = require('casper').selectXPath;
        if (this.exists(x(selectorNth))) {
            this.echo('found link: '+this.getElementInfo(x(selectorNth)).tag);
            this.thenClick(x(selectorNth));
            this.wait(json.clickSpeed,function() {  //changed based on network speed
            });
            if(this.exists(x(selectorNNth))) {
                return this.clickWhileSelector(selector, i + 1, moreSelector);
            }
        }
        else{
            casper.echo('点击失败, clickWhileSelector Didnt find link');
        }
    });
}

casper.setMultiKeys = function(selector,num,i,moreSelector){
    i = i || 1;
    moreSelector = moreSelector||'';
    var j=i+1;
    var eInput = '//input[@class="text qty atg_store_numericInput  error"]';
    selectorNth = selector+'[' + i + ']'+moreSelector;
    selectorNNth = selector+'[' + j + ']'+moreSelector;
    var subNum = num;

    this.waitFor(function check() {
        return this.exists(x(selectorNth))
    },function then(){
        this.echo('Found input: ' + this.getElementInfo(x(selectorNth)).tag);
        casper.echo("Num: " + num);
        this.sendKeys(x(selectorNth), ""+num);
        if (json.capture) {
            this.capture("./capture2/3-3-"+num+".png");
        }
    });
    this.then(function () {
        var style =this.evaluate(function () {
            return document.querySelector('div[class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable"][aria-labelledby="ui-dialog-title-takeAwayDialog"]').getAttribute('style');
        });
        this.echo("style:"+style);
        //exist error number
        if(style!==null && style.indexOf("block")!== -1){
            //*[@id="takeAwayDialog"]/table/tbody/tr[2]/td[2]/div/span
            this.thenClick(x('//*[@id="takeAwayDialog"]//span[@class = "closeD"]'));
            this.echo("下单数量不可用, 开始重试减半数量！", 'INFO');
            subNum = Math.ceil(subNum/2);
            this.echo("正在设置数量！");
            if(json.capture){
                this.capture("./capture2/3-4-"+subNum+".png");
            }
            this.reload();
            if(subNum!==1){
                return this.setMultiKeys(selector,""+subNum,i,moreSelector);
            }
        }
        if(this.exists(x(selectorNNth))) {
            return this.setMultiKeys(selector,num,j,moreSelector);
        }
        return this.echo('Set Keys Finished.');
    });
}

casper.sendEmail = function send(title,content) {
    var cp = require('child_process');
    this.echo("正在发送邮件...")
    cp.execFile('/usr/bin/python', ['sendmail.py',json.mailHost,json.mailPort,json.mailSender,json.mailPassword,json.mailReceiver,title,content],function(error,stdout,stderr){
        if(error) {
            console.info('stderr : '+stderr);
        }
    });
}